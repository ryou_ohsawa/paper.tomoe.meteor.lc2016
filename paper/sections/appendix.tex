\section{Validation of the EMCA Calculation}
\label{sec:emca_validation}

\subsection{Travel Distances}
\label{sec:travel_distances}
In the present formalism, we assume that an incident meteor never fades away inside the meteor shell. This assumption may bring an overestimate of a rate of meteors with a long travel distance, and an EMCA could be overestimated accordingly. Figure \ref{fig:travel} shows the distribution of the travel distances of the meteors for System 1 with $\alpha = 60\arcdeg$. The distribution neither depends on the systems nor elevation angles. The fraction of the meteors with a travel distance longer than $400\,\mathrm{km}$ was $\lesssim 10\%$. Thus, we concluded that the assumption had virtually no effect on the present results.

\begin{figure*}[p]
  \plotone{figs/travel.pdf}
  \caption{Distribution of the travel distances of observable meteors. The filled bars show the histogram the travel distances for System 1 with an elevation angle $\alpha = 60\arcdeg$. The cumulative distribution is described by the solid step line.}
  \label{fig:travel}
\end{figure*}


\subsection{Position Angles}
\label{sec:position_angles}
Artificial patterns are found in the distribution of position angles on the focal plane. Position angles (PA) of meteors for System 2 are summarized in Figure~\ref{fig:pa}. Dips around $\mathrm{PA} = -90\arcdeg$ and $90\arcdeg$ in the top panel of Figure~\ref{fig:pa} are attributed to the alignment of the detectors. A fraction of meteors with $\mathrm{PA} \sim {{\pm}180\arcdeg}$ increased with decreasing elevation angle. Thus, a large fraction of meteors run from the zenith to the nadir in a field-of-view. The observations on April 11 and 14 were carried out with elevation lower than 38\arcdeg. Similar distributions of the PAs were confirmed in the observations. Our calculation is qualitatively consistent with the observations. The degree of the concentration depends on the distribution of the incident angle $i$. Since it was difficult to constrain the distribution of $i$ from the observations, we did not discuss the PA distribution of the meteors in the paper.

\begin{figure*}[p]
  \plotone{figs/angle_sys2.pdf}
  \caption{Distributions of the position angles of mock meteors. A position angle of zero corresponds to a meteor moving toward the zenith, or upward in the field-of-view.}
  \label{fig:pa}
\end{figure*}


\subsection{Depth of the Meteor Shell}
\label{sec:depth_meteor_shell}
We assume that the top and bottom boundaries of the meteor shell were at $120$ and $80\,\mathrm{km}$, respectively. They are chosen as nominal values \citep{jenniskens_meteor_2006}. An appearing and disappearing altitude of a meteor, however, depend on the size or composition of the meteoroid. We calculate the EMCAs for $H_1 = 60\,\mathrm{km}$ and $H_2 = 140\,\mathrm{km}$, the results are almost the same. Thus, we conclude that the choice of $H_1$ and $H_2$ has little impact on the present results.


\subsection{Reduced Efficient Meteor-Collecting Area}
\label{sec:rmca}
The EMCA defined in Equation~(\ref{eq:mca}) does not take into account the apparent brightness of incident meteors and atmospheric extinction. A practical meteor-collecting area is given by reducing $A$ as
\begin{equation}
  \label{eq:red}
  A^\mathrm{red} =
  \frac{A_0}{N_0}\left(\frac{100\,\mathrm{km}}{H_2}\right)^2
  \sum^N_i r^{-5\log_{10}\frac{d_i}{100\,\mathrm{km}}-\epsilon_i},
\end{equation}
where $r$ is the population index, $d_i$ is the distance to the $i$-th meteor, and $\epsilon_i$ is the amount of extinction of the $i$-th observable meteor. The reduced effective meteor-collecting area (\textit{hereafter}, referred as to RMCA) corresponds to the area defined in Equation~(1) of \citet{koschack_determination_1990}. For the sake of simplicity, we adopt $\epsilon_i = 0$ in the rest of the paper.

We calculate the RMCAs for Systems 2. The position angle $\phi$ is fixed at zero. Figure~\ref{fig:rmca} shows the RMCA for $r=2.0$, $2.5$, $3.0$, $3.5$, and $4.0$ against an elevation angle. While the RMCA is almost the same as the EMCA when the elevation angle $\alpha$ is larger than about $80\arcdeg$, the differences between the EMCA and RMCA become significant at low elevation angles.

The differences increase as $r$ becomes large. \citet{hawkins_influx_1958} and \citet{cook_flux_1980} independently obtained $r \simeq 3.4$ for sporadic meteors with $M_\mathrm{pg} < {+}5$ and ${+}7 < M_\mathrm{pg} < {+}12$, respectively. Thus, we safely assume $r > 3.0$ for visible sporadic meteors. In such cases, the RMCA is largest at the zenith. This does not depend on systems. In observations of sporadic meteors, a monitoring observation close to the zenith is favored. When $r$ is larger than about $3.0$, the RMCA monotonically decreased with decreasing elevation angle in contrast to the EMCA. The RMCA is well-approximated by
\begin{equation}
  \label{eq:approxAred}
  \tilde{A}^\mathrm{red} =
  Ar^{-5\log_{10}\frac{\langle d \rangle}{100\,\mathrm{km}}},
\end{equation}
where $\langle d \rangle$ is the averaged distance to the MCVs. Since the field-of-view is sufficiently narrow, the distances to the meteors are approximately identical. This justifies Equations (\ref{eq:expnum}) and (\ref{eq:approxAred}).

\begin{figure*}[p]
  \plotone{figs/rmca.pdf}
  \caption{Reduced effective meteor-collecting areas (RMCAs) in units of $\mathrm{km^2}$ against an elevation angle $\alpha$. The symbols indicate an assumed meteor index value $r$. The gray dashed line indicates the EMCA for System 3 as a reference.}
  \label{fig:rmca}
\end{figure*}

Equation~(\ref{eq:red}) indicates that an expected number of meteors can be given by $\tilde{n} = A^\mathrm{red}N_0r^\mathcal{M}$, which is a function of the meteor index $r$, the elevation angle $\alpha$, and the limiting magnitude $\mathcal{M}$. Figure~\ref{fig:rmca} illustrates the dependence of $A^\mathrm{red}$ on the meteor index $r$; the RMCA ratio at $\alpha = 30\arcdeg$ to $\alpha = 90\arcdeg$ decreases from 1.74 to 0.63 when the meteor index $r$ increases from 2.0 to 4.0. Thus, comparing the number of meteors detected in different elevation angles provides an estimate of the meteor index $r$ without measuring the brightness of the meteors. Figure~\ref{fig:index} shows an application of this method. The orange crosses show the numbers of meteor events in 3\,minute on April 11, 2016\footnote{The data on April 14, 2016 are not shown, since large part of the data with $\alpha > 30\arcdeg$ were obtained in bad conditions.}. The number of detections decreases with decreasing elevation angle. The lines indicate the expected number of meteor detections for different $r$ values. The limiting magnitudes are estimated from neighboring stars. The observations are well explained by the expectations with $r=3.0$--$4.0$, consistent with previous studies \citep{cook_flux_1980,hawkes_television_1975,hawkins_influx_1958}. This confirms that the present results are self-consistent.

\begin{figure*}[p]
  \plotone{figs/meteor_index_by_count.pdf}
  \caption{Deriving the meteor index $r$ by counting the number of meteors. The orange crosses shows the number of the meteors in 3\,minute against the elevation angle in observation. The lines show the expected number of detections for different $r$ values.}
  \label{fig:index}
\end{figure*}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
