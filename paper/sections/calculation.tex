\subsection{System Efficiency}
\label{sec:syseff}

\begin{figure*}
  \centering
  \plotone{figs/tomoepm_collectarea.pdf}
  \caption{Definition of the meteor detectable volume. The blue dot is the location of a telescope, which is observing a sky at the elevation $\alpha$. The thin blue region indicates the meteor detectable layer. The blue cross-hatched region indicates the meteor detectable volume. The thick blue region is the test region, where meteors are randomly generated. A generated meteor with the incident angle $\theta$ is indicated by the red arrow.}
  \label{fig:schematic-collect}
\end{figure*}

The meteor-collecting power of a observing system is usually evaluated in units of area. \citet{koschack_determination_1990} evaluated the effective meteor-collecting power from the area of the field-of-view projected onto a meteor level at an altitude of $100\,\mathrm{km}$ weighted by distance. In observation with naked eyes (visual observation), a field-of-view was as large as about $52.5\arcdeg$ \citep{koschack_determination_1990}. The estimated effective meteor-collecting area (\textit{hereafter}, EMCA) in visual observation is $24,400\,\mathrm{km^2}$ when observing at zenith, whereas that area becomes $20,770\,\mathrm{km^2}$ when observing with an elevation angle of $50\arcdeg$, in the case that a meteor population index\footnote{The number of meteors brighter than a magnitude $m$ in visible wavelengths should follow $N({<}m) \propto r^m$.} $r$ is 3.5. In Koschack's calculation, the region where meteors are observable is approximated by a thin spherical shell. Hereafter, Koschack's formalism is referred as to the thin shell approximation.

In meteor observations with telescopes, the field-of-view is usually smaller than 10\arcdeg. In such cases, the thin shell approximation is not valid. \citet{kresakova_activity_1955} evaluated the EMCA by statistically estimating the averaged angular length of meteors. This method requires a large number of meteors. Since the efficiency in meteor detection with \tomoepm changes with elevation and \tomoepm has the multiple image sensors, their method is not applicable to our observations. Thus, we evaluated the EMCA of \tomoepm by a Monte Carlo simulation.

Figure~\ref{fig:schematic-collect} schematically describes the configuration of calculation. When \revise{20180710}{dust grains} penetrate into the upper atmosphere, they are heated by interaction and become bright in optical wavelengths at a certain altitude, observed as meteors. Then, when the \revise{20180710}{grain} penetrates into a lower atmospheric layer, they cease to be bright. Here, we define an upper and lower limit of an altitude where meteors are observable in optical wavelengths by $H_2$ and $H_1$, respectively. Thus, the region where meteors are observable in optical wavelengths is approximated by a spherical shell with a depth of $H_2{-}H_1$ (\textit{hereafter}, meteor shell). In Figure~\ref{fig:schematic-collect}, the meteor shell is indicated by the blue shaded region.

The region captured by a camera with a single rectangular field-of-view is defined by
\begin{equation}
  \label{eq:mov}
  \vec{x} \in \left\{~
    c_0\vec{n}_0+c_1\vec{n}_1+c_2\vec{n}_2+c_3\vec{n}_3
  ~\middle|~
    c_m > 0 ~\text{for}~ m=0,1,2,3
  ~\right\},
\end{equation}
where $\vec{n}_m (m=0,1,2,3)$ is a unit vector to point a corner of the field-of-view. The intersection of the meteor shell and the field-of-view is defined as a meteor-collecting volume (MCV). The MCV is approximated by a truncated pyramid as shown by the cross-hatched region in Figure~\ref{fig:schematic-collect}. This approximation does not affect results significantly as long as the dimension of the field-of-view is smaller than about $1\arcdeg$. The distance to the MCV is represented by the distance to its geometric center. For a system with multiple image sensors such as \tomoepm, the union of the MCVs for each sensor is defined as the MCV of the system.

An incident meteor is defined by two properties: an incident point $\vec{p}$ and a pair of incident angles $(\omega,i)$. The incident point $\vec{p}$ means the location of \revise{20180710}{a dust grain} when they enter the meteor shell. The incident angles correspond to the velocity of \revise{20180710}{a grain} relative to the Earth. The angle $\omega$ is an azimuthal angle, while the angle $i$ is an incident angle measured from the normal vector of the meteor shell at $\vec{p}$. Incident meteors are illustrated by the orange arrows in Figure~\ref{fig:schematic-collect}. The evolution of the velocity by interaction with atmosphere is not taken into account. Thus, we assume that the direction of a meteor's velocity is constant.

Incident meteors are randomly generated on the test region, which is a sufficiently large part of the meteor shell, with a surface density of $\Sigma$ at an altitude of $H_2$. An effective meteor-collecting area (EMCA) at a nominal altitude of $100\,\mathrm{km}$ is defined by
\begin{equation}
  \label{eq:mca}
  A = \frac{N}{\Sigma}\left(\frac{100\,\mathrm{km}}{H_2}\right)^2
  = N\frac{A_0}{N_0}\left(\frac{100\,\mathrm{km}}{H_2}\right)^2,
\end{equation}
where $N$ is the number of the meteors which enter the MCV, $A_0$ is the total area where the meteors are generated, $N_0$ is the total number of the generated meteor. We developed a code to calculate an EMCA for a given observing configuration. The shape, the elevation angle $\theta$, and the position angle $\phi$ of pointing are given. Incident meteors are generated until $N$ exceeds a threshold $\hat{N}$. Since the detections are given by a Poisson process, the statistical error of $A$ becomes negligible for sufficiently large $N_0$.

When the field-of-view is as large as ${\sim}50\arcdeg$ in diameter, the depth of the meteor shell becomes negligible. Thus the EMCA calculated here becomes asymptotically identical to that in the thin shell approximation.

As the simplest case, we assumed that the number density of \revise{20180710}{interplanetary dust} around the Earth was uniform and the velocity of the \revise{20180710}{interplanetary dust grains} relative to the Earth was isotropic. Thus, the distribution of $\vec{p}$ was uniform on the top surface of the meteor shell, the distribution of $\omega$ distributed uniformly in $[0,\,2\pi)$, and the distribution of $i$ was set to be proportional to $\cos{i}$. In calculation, we adopted $H_1 = 80\,\mathrm{km}$ and $H_2 = 120\,\mathrm{km}$. The EMCA does not significantly depend on the choice of $H_1$ and $H_2$ (see, \ref{sec:depth_meteor_shell}). The threshold $\hat{N}$ was set to $100,000$ so that the statistical errors become negligible. Note that the effective meteor-collecting area calculated based on our assumption may contain a systematic error since the radiant distribution of sporadic meteor is not uniform \citep{jones_radiant_1994}, and may be incorrect especially for meteor showers.

\begin{figure*}
  \centering
  \plotone{figs/meteor_collectiong_area.pdf}
  \caption{Time variation in the meteor-collecting areas on April 11 (the red circles) and April 14 (the blue triangles).}
  \label{fig:collecting-area}
\end{figure*}
The EMCAs of \tomoepm estimated with a Monte-Carlo simulation are shown in Figure~\ref{fig:collecting-area}. Random errors are as small as the symbols. Figure~\ref{fig:collecting-area} indicates that the meteor-collecting area of \tomoepm changed from about $1,000\,\mathrm{km}$ to $10,000\,\mathrm{km}$ with elevation. \revise{20180710}{Note that the EMCAs calculated here do not include the effect of the distance between the observer and meteors. When the elevation angle is low, a typical distance to meteors becomes large and faint meteors are not detected. Thus, the number of the detected meteors tends to decrease with decreasing elevation angle. Figure~\ref{fig:evrates} shows that the number of the detected meteors peaked around 15:00 UT on April 11. Detailed discussion is presented in \ref{sec:rmca}. The meteor travel distances and a possible bias in the meteor arrival directions are discussed in \ref{sec:travel_distances} and \ref{sec:position_angles}, respectively. }


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
