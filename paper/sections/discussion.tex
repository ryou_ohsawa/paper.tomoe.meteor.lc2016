\subsection{Absolute Magnitude Distribution}
\label{sec:absmags}
\begin{figure*}[tp]
  \centering
  \plotone{./figs/hist.pdf}
  \caption{Distributions of the absolute magnitudes of the meteors detected by \tomoepm. The top and bottom panels respectively shows the distributions on April 11 and 14, 2016.}
  \label{fig:hist}
\end{figure*}
The observed video-rate magnitudes $m_{V}$ were converted into absolute magnitudes $M_{V}$, which correspond to $V$-band magnitudes at the distance of $100\,\mathrm{km}$, by assuming that the distances to meteors were identical to those to the meteor detectable volume:
\begin{equation}
  \label{eq:mag100km}
  M_V = m_V
  + 5.0\log_{10}\frac{D(\alpha)}{100\,\mathrm{km}}.
\end{equation}
Figure~\ref{fig:hist} shows the distributions of the absolute magnitudes. Meteros of $M_V \lesssim 10$ and $9\,\mathrm{mag}$ were detected on April 11 and 14, respectively. The histograms increase exponentially with increasing magnitudes, and then start to decrease at magnitudes of 8th or 9th. The decrease is basically attributed to sensitivity limits. Detailed discussion is given in Section~\ref{sec:lf_analysis}.

\citet{jacchia_physical_1955} derived an equation to convert the magnitude of a meteor to the mass of a meteoroid, which were calibrated based on observations with Super-Schmidt cameras. A modified equation was given by \citet{jenniskens_meteor_2006}, which derives the mass of a meteoroid from the $V$-band magnitude of a meteors:
\begin{equation}
  \label{eq:jenniskens}
    \log_{10}{M_g} = 6.31 - 0.40M_V
     - 3.92\log_{10}V_\infty - 0.41\log_{10}\sin(h_r),
\end{equation}
where $M_g$ is the mass of the meteoroid in units of grams, $M_V$ is the $V$-band magnitude of the meteor, $V_\infty$ is the incident velocity of the meteoroid in units of $\mathrm{km\,s^{-1}}$, and $h_r$ is the elevation of the radiant point of the meteor. By assuming that $V_\infty$ is $30\,\mathrm{km\,s^{-1}}$ and $h_r$ is $90\arcdeg$, the mass of the meteors we detected ranges from about $8.3{\times}10^{-2}\,\mathrm{g}$ ($4\,\mathrm{mag}$) to about $3.3{\times}10^{-4}\,\mathrm{g}$ ($10\,\mathrm{mag}$).


\subsection{Luminosity Function Analysis}
\label{sec:lf_analysis}
Generally, a luminosity function of visible meteors is well approximated by an exponential function \citep{hawkins_influx_1958}:
\begin{equation}
  \label{eq:lf}
  \log_{10} N({<}M) = \log_{10}N_0 + M\log_{10}r,
\end{equation}
where $N({<}M)$ and $N_0$ are the event rates of meteors brighter than $M$-th and zero-th magnitude, respectively.

The present results are a composite of the observations in different conditions in terms of the meteor-collecting area and the limiting magnitudes. Thus, a naive fitting of the apparent magnitude distribution to derive luminosity functions may bring biased results. We introduce a statistical model to robustly estimate the slope parameter $r$ and the meteor rate $\log_{10}N_0$.

We assume that the luminosity function of meteors exactly follows Equation~(\ref{eq:lf}) and observational conditions are constant within the observation unit. The number of detectable meteors per observation unit is expected by
\begin{equation}
  \label{eq:expnum}
  \left\{
  \begin{array}{ll}
  \log_{10}\tilde{n}^i &= \log_{10}tN^i_0A_i + \mathcal{M}^i\log_{10}r \\
  \log_{10}N^i_0 &= \log_{10}N_0 + \eta(T^i - \mathrm{15:00}_\mathrm{UTC})
  \end{array},\right.
\end{equation}
where $t$ is the duration of an observation unit, $A_i$ is the meteor-collecting area of the $i$-th observation unit in units of $\mathrm{km^2}$, $\mathcal{M}^i$ is a limiting magnitude in the $i$-th observation unit, $\eta$ is a parameter to describe the diurnal variation \citep{hawkins_variation_1956,hawkins_influx_1958,fentzke_latitudinal_2009} in units of $\mathrm{dex\,hour^{-1}}$, and $T_i$ is the observation time of the $i$-th observation unit in hours in UTC. From Equation~(\ref{eq:lf}), expected magnitudes of detectable meteors in the $i$-th observation unit $\tilde{M}^i$ should follow
\begin{equation}
  \label{eq:magexp}
  \mathcal{M}^i - \tilde{M}^i \sim \mathrm{exponential}(\beta),
\end{equation}
where $\beta$ is a shape parameter of the exponential distribution, given by $\beta = \log{r}$.

\begin{figure*}[tp]
  \centering
  \plotone{./figs/model_fitting_r_diurnal.pdf}
  \plotone{./figs/model_fitting_log10_N0_diurnal.pdf}
  \plotone{./figs/model_fitting_eta_diurnal.pdf}
  \caption{Probability density funciton of the fitting parameters, $r$, $\log_{10}N_0$, and $\eta$, are shown in the top, middle, and bottom panels, respectively. The vertical lines with numbers indicate the posterior mean values.}
  \label{fig:modelfit}
\end{figure*}
\begin{table*}
  \centering
  \caption{Statistical Inference with the Simple Model}
  \label{tab:modelfit}
  \begin{tabular}{cccccc}
    \hline\hline
    & Parameter & Mean & 2.5\% & 50\% & 97.5\% \\ \hline
    2016-04-11
    & $r$            &$\phm3.24$&$\phm3.01$&$\phm3.23$ &$\phm3.50$ \\
    & $\log_{10}N_0$ &   $-5.59$&   $-5.91$&   $-5.59$ &   $-5.30$ \\
    & $10^2\eta$     &$\phm1.16$&   $-0.30$&$\phm1.16$ &$\phm2.62$ \\
    \hline
    2016-04-14
    & $r$            &$\phm3.08$&$\phm2.74$&$\phm3.06$&$\phm3.50$ \\
    & $\log_{10}N_0$ &   $-5.34$&   $-5.82$&   $-5.33$&   $-4.93$ \\
    & $10^2\eta$     &$\phm0.50$&   $-1.92$&$\phm0.50$&$\phm2.91$ \\
    \hline
  \end{tabular}
\end{table*}

The parameters are optimized to match $n^i \sim \tilde{n}^i$ and $M^i \sim \tilde{M}^i$. The optimization is carried out with \texttt{Stan} \citep{carpenter_stan:_2016}, which is a software for the Bayesian statistical inference with MCMC sampling \citep{carpenter_stan:_2016,homan_no-u-turn_2014}. The posterior probability distributions are shown in Figure~\ref{fig:modelfit}. The posterior mean values and the 95\% confidence intervals of the parameters are listed in Table~\ref{tab:modelfit}. No significant differences are detected between the results on April 11 and 14. The results suggest that the slope parameter is ${\sim}3.1\pm{0.4}$ and the meteor rate is about $-5.5{\pm}0.5$.

The data on April 11 marginally suggest a positive $\eta$ value, while the increase in the meteor rate is not confirmed on April 14. The present result, $\eta$ of ${\sim}1{\times}10^{-2}\,\mathrm{dex\,hours^{-1}}$, corresponds the increase by 30\% in 12 hours. The increase was much smaller than reported in previous studies \citep{murakami_annual_1955,kero_2009-2010_2012}. This could be in part attributable to our assumption in calculating the EMCA: the radiant distribution of meteors is uniform. Further observations are required to confirm the non-ditection of the diurnal variation with \tomoepm.

\begin{figure}[tp]
  \centering
  \plotone{./figs/meteoroid_flux.pdf}
  \caption{Comparison of luminosity functions obtained in different observations. The present result is indicated by the blue region.}
  \label{fig:luminosity_functions}
\end{figure}

We compare the present result with luminosity functions in literature in Figure~\ref{fig:luminosity_functions}. The purple solid line indicates the luminosity function derived by \citet{hawkins_influx_1958} based on surveys with Super-Schmidt cameras. \citet{hawkins_influx_1958} suggested that the $r=3.4{\pm}0.2$ and $\log_{10}N_0 = -5.2$ using meteors brighter than about $4\,\mathrm{mag}$. The green dashed line indicates the luminosity function provided by \citet{cook_flux_1980} using a 10-m reflector and phototubes. The slope parameter they derived was about $3.41$. The blue doted-dashed line and orange double-dotted-dashed line respectively show the luminosity functions in \citet{clifton_television_1973} and \citet{hawkes_inexpensive_1973}. They observed meteors using television systems. Since the brightness of meteors were not directly measured in \citet{clifton_television_1973}, we use the luminosity function re-calibrated by \citet{cook_flux_1980}. \citet{hawkes_television_1975} suggested that $r \sim 2.5$, while \citet{clifton_television_1973} suggested that $r \sim 3.4$. The red triple-dotted-dashed line shows the luminosity function of fireballs \citep{hawkins_relation_1959}, suggesting that $r \sim 2.5$. Here, we tentatively assume that the conversion factor from photographic to visual magnitude is ${+}1.0\,\mathrm{mag}$. The present result is shown by the blue region. The height of the region represents the uncertainties of the parameters.

The present observations constrain the luminosity function between about $+4$ to $+10\,\mathrm{mag}$, which is the deepest among the imaging observations. The slope parameter in the present result is roughly consistent with the other observations except for \citet{hawkins_relation_1959}. The meteor rate $N_0$ is by a factor of ${\sim}2.5$ lower than the value reported in \citet{hawkins_influx_1958}. This can be attributed to a seasonal variation in the sporadic meteor rate; The number of sporadic meteors in March and April is about a half of the annual average rate \citep{hawkins_variation_1956,kero_2009-2010_2012,murakami_annual_1955}. The meteor rate of the present result is by a factor of ${\sim}30$ lower than that of \citet{cook_flux_1980}. This difference could be in part attributable to the difference in the assumed EMCAs. \citet{cook_flux_1980} assumed that the EMCA was $3\,\mathrm{km^2}$, which was basically derived with the thin shell approximation and could be underestimated. Generally, the present result is consistent with the luminosity functions in literature. \citet{cook_flux_1980} suggested that the luminosity function of meteors was well approximated by a single slope power-law function from $-2.4$ to $+12\,\mathrm{mag}$. The present result is in line with the Cook's suggestion.


\subsection{System efficiency comparison between \tomoepm and \tomoe}
The operation of \tomoepm was completed in 2016. Observations with \tomoe started in February, 2018. Here, we compare the EMCAs between \tomoepm and \tomoe, to evaluate the efficiency in the meteor observations with \tomoe.

The EMCAs are calculated out for three systems. The shape of their \revise{20180710}{fields-of-view} are illustrated in Figure~\ref{fig:sys}. System 1 has a single field-of-view with dimensions of $39\arcmin.7{\times}22\arcmin.4$, corresponding to that of one CMOS sensor. System 2 emulates \tomoepm, a wide-field camera equipped with 8 CMOS image sensors mounted on 105\,cm Kiso Schmidt telescope \citep{sako_development_2016}. System 2 has eight image sensors, each of which has a field-of-view with dimensions of $39\arcmin.7 {\times} 22\arcmin.4$. System 3 is a counterpart of \tomoe. The field-of-view of System 3 is composed of 84 segments. The size of each segment is the same in Systems 1 and 2.

The MCVs with an elevation angle of $35\arcdeg$, $60\arcdeg$, and $85\arcdeg$ are illustrated in Figure~\ref{fig:mcv_sys1}. The position angles of the field-of-view are fixed. Both the size of the MCV and the distance to the MCV increase with decreasing elevation angle.

\begin{figure*}
  \plotone{figs/systems.pdf}
  \caption{\revise{20180710}{Fields-of-view} of the three systems. The violet cross-hatched region indicates the field-of-view of System 1. The green empty rectangles indicate the field-of-view of System 2. The blue filled rectangles indicate the field-of-view of System 3.}
  \label{fig:sys}
\end{figure*}

\begin{figure*}
  \plotone{figs/mcv_sys1.pdf}
  \plotone{figs/mcv_sys2.pdf}
  \plotone{figs/mcv_sys3.pdf}
  \caption{Schematic view of meteor-collecting volumes (MCV). The top, middle, and bottom plots respectively show the MCVs for Systems 1, 2, and 3. The labels ``A'', ``B'', and ``C'' indicate the MCVs for elevation angles of $85\arcdeg$, $60\arcdeg$, and $35\arcdeg$, respectively. The blue dots indicate the location of the observing system. The green solid curve in Panels B indicates the sea level. The gray dashed lines in Panels B respectively show the top and bottom surface of the meteor sphere.}
  \label{fig:mcv_sys1}
\end{figure*}

The EMCAs for the three systems are calculated for elevation angles from $10\arcdeg$ to $90\arcdeg$ at $5\arcdeg$ intervals. Figure~\ref{fig:emca} shows the dependence of the EMCA on the elevation angle $\alpha$. The EMCAs at zenith are about $43$, $281$, and $630\,\mathrm{km^2}$ for System 1, 2, and 3, respectively. At $\alpha = 10\arcdeg$, the EMCAs increase to about $1.1{\times}10^3$, $7.8{\times}10^3$, and $25.8{\times}10^3\,\mathrm{km^2}$ for System 1, 2, and 3, respectively. The EMCAs for Systems 1, 2, and 3 similarly increase with decreasing elevation angle. The dependence of the EMCA on $\alpha$ is well approximated by $\sin^{-2}\alpha$. \revise{20180710}{Note that the EMCAs, which do not include the effect of the distance to meteors, do not reflect the expected number of meteors detected. Refer to \ref{sec:rmca} for detailed discussion.}

\begin{figure*}
  \plotone{figs/emca.pdf}
  \caption{Effective meteor-collecting areas (EMCAs) in units of $\mathrm{km^2}$ against an elevation angle $\alpha$. The error bars are as small as the line width.}
  \label{fig:emca}
\end{figure*}

The EMCAs for Systems 2 is about 7 times larger than that of System 1. This increase is almost proportional to the number of the image sensors. The efficiency of System 3 is about \revise{20180710}{two times} larger than that of System 2. Since the detectors of \tomoe are tiled in the image circle of the telescope, a large fraction of meteors will be detected first in peripheral detectors; the detectors around the center of the field-of-view have little contribution to the meteor-collecting efficiency.

\begin{table*}
  \centering
  \caption{EMCA for Square \revise{20180710}{Fields-of-View}}
  \label{tab:emca_simple}
  %\begin{tabular}{p{9.0em}p{4.8em}p{4.8em}p{4.8em}p{4.8em}p{4.8em}p{4.8em}}
  \begin{tabular}{p{9.0em}cccccc}
    \hline\hline
    & \multicolumn{1}{c}{$10\arcmin{\times}10\arcmin$}
    & \multicolumn{1}{c}{$20\arcmin{\times}20\arcmin$}
    & \multicolumn{1}{c}{$30\arcmin{\times}30\arcmin$}
    & \multicolumn{1}{c}{$40\arcmin{\times}40\arcmin$}
    & \multicolumn{1}{c}{$50\arcmin{\times}50\arcmin$}
    & \multicolumn{1}{c}{$60\arcmin{\times}60\arcmin$}
    \\\hline
    Side Length (\arcmin)
    & $10$
    & $20$
    & $30$
    & $40$
    & $50$
    & $60$
    \\
    FOV area ($\mathrm{deg}^2$)
    & $0.03$
    & $0.11$
    & $0.25$
    & $0.44$
    & $0.69$
    & $1.00$
    \\
    EMCA ($\mathrm{km^2}$)
    & $14.6$
    & $27.2$
    & $41.4$
    & $55.1$
    & $69.2$
    & $83.2$
    \\
    Projected Area\tablenotemark{a} ($\mathrm{km^2}$)
    & $0.09$
    & $0.34$
    & $0.76$
    & $1.35$
    & $2.12$
    & $3.05$
    \\
    Event Rate\tablenotemark{b} ($\mathrm{hour^{-1}}$)
    & $22$
    & $45$
    & $68$
    & $90$
    & $114$
    & $136$
    \\
    \hline\hline
  \end{tabular}
  \tablenotetext{a}{The area of the field-of-view projected on the sphere at an altitude of $100\,\mathrm{km}$.}
  \tablenotetext{b}{Estimated meteor event rates for meteors brighter than $10$ magnitude.}
\end{table*}

To investigate the dependence of the EMCA on the field-of-view, we calculate the EMCA for simple systems which have a single square field-of-view pointing at the zenith. The \revise{20180710}{side lengths} of the \revise{20180710}{fields-of-view} are $10\arcmin$, $20\arcmin$, $30\arcmin$, $40\arcmin$, $50\arcmin$, and $60\arcmin$. Calculated EMCAs are listed in Table~\ref{tab:emca_simple} as well as the areas of the \revise{20180710}{fields-of-view} projected on the surface at an altitude of $100\,\mathrm{km}$ above the sea level (\textit{hereafter}, referred as to projected \revise{20180710}{fields-of-view}).

The EMCA is larger than the area of the projected field-of-view. In the case of the $60\arcmin{\times}60\arcmin$ system, the EMCA is larger by a factor of about 30. This simply illustrates the thin shell approximation is not appropriate for systems with small \revise{20180710}{fields-of-view}.

The EMCAs are almost proportional to the \revise{20180710}{side lengths} of the \revise{20180710}{fields-of-view}, rather than the areas of the \revise{20180710}{fields-of-view}. \revise{20180710}{The meteor-collecting efficiency, in general, depends on the surface area of the MCV. For a narrow field-of-view, the surface area of the MCV is approximately proportional to the side length of the field-of-view. This simply explains the dependence of the EMCA on the side length; The EMCA of narrower field-of-view systems does not decrease as with the area of the field-of-view. This is generally consistent with the fact that the EMCA of System 3 is about two times larger than that of System 2; The summation of the most outer circumference sides of System 2 is about $11.3\arcdeg$, while that of System 3 is about $19.5\arcdeg$.}

The expected number of detected meteors per hour is estimated by $A N_0 r^{\mathcal{M}}$, where $\mathcal{M}$ is a limiting magnitude, $r$ is a meteor index, $A$ is an EMCA, and $N_0$ is a rate of meteors brighter than zeroth magnitude. Here, we adopt $r=3.4$ and $\log_{10}N_0 = -5.1$ \citep{hawkins_influx_1958}. Table~\ref{tab:emca_simple} lists the event rate calculated for $\mathcal{M} = 10\,\mathrm{mag}$ in optical wavelengths. A system with a $1\arcdeg{\times}1\arcdeg$ field-of-view and a limiting magnitude of ${\sim}10$ will detect more than 100 meteors per hour. This result encourages meteor survey observations with wide-field cameras mounted on large aperture telescopes (e.g. Subaru/HSC \citep{miyazaki_hyper_2012}).

\tomoe is expected to detect more than about 2,000 faint meteors a night. Since meteors will run across multiple detectors of \tomoe, faint meteors are more robustly detected with \tomoe than \tomoepm. \tomoe will be an ideal instrument to investigate variations or structures in the luminosity function of meteors brighter than about ${+}10\,\mathrm{mag}$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
