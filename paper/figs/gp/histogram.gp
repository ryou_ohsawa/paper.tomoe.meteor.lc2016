#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 14.0cm,11.0cm font "Ubuntu,16"
set output '../hist.pdf'

set xr [2:12]
set yr [0:280]
set xtics 0,2,20 format ''
set ytics auto format '%3.0f'
set mxtics 4
set mytics 1

set lmargin 8.0
set rmargin 1.5
set bmargin 3.5
set tmargin 0.0
unset xlabel
unset ylabel
set label 1 'Number of Meteros' font ',20' \
center at graph 0, screen 0.5 offset -6,0 rotate by 90

set grid
set tic front
set bar 0
set key left Left rev font ',16'

bin(x,w) = w*int(x/w)
set origin 0,0
set size 1
set multiplot
set size 1,0.55
set origin 0,0.43
plot \
   '../data/meteor_event_log_20160411.tbl' \
   u (bin($2-5*log10($11/100.),0.4)):(1) s freq w boxes \
   fs solid 0.5 lc rgb 'coral' t '2016-04-11'

set origin 0,0
set xtics 0,2,20 format '%.0f'
set xlabel 'Magnitude (at 100km, V-band)' font ',20'
plot \
   '../data/meteor_event_log_20160414.tbl' \
   u (bin($2-5*log10($11/100.),0.4)):(1) s freq w boxes \
   fs solid 0.5 lc rgb 'royalblue' t '2016-04-14'
###
unset multiplot

