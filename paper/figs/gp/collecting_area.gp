#!/usr/bin/gnuplot
set terminal pdfcairo size 16cm,10.0cm enh \
  font "Ubuntu, 16" dashed background 'white'
set encoding iso

set size 1
set origin 0,0
set tics front

set lmargin 7.5
set rmargin 2
set tmargin 2.0
set bmargin 3.2

set boxwidth 1
set key top left samplen 2 font ',16'

## 20160411+20160414
set output '../meteor_collectiong_area.pdf'
F='../data/meteor_collect_area_20160411.tbl'
G='../data/meteor_collect_area_20160414.tbl'

set timefmt '%H:%M'
set xdata time
set xrange ['11:45':'19:15']
set yrange [0:13]
set xtics auto timedate format '%H:%M' font ',16'
set ytics 0,2,20 format '%3.0f'
set mxtics 4
set mytics 4
set bar 0

set label 1 '({\327}10^{3})' right at graph 0, graph 1 \
font ',16' offset 1.4,0.8

set xlabel 'Observation Time (UT)' font 'Ubuntu, 20' offset 0,0.0
set ylabel 'Effective Collecting Area (km^2)' font 'Ubuntu, 20'

plot \
   F u 2:($4/1e3) w p pt 7 ps .5 lw 2 lc rgb 'coral' t '2016-04-11', \
   G u 2:($4/1e3) w p pt 9 ps .5 lw 2 lc rgb 'royalblue' t '2016-04-14'
###

unset output
