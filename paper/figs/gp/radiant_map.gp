if (!exists("n")) { n = 0 }

if (ARGC>0) {
sys=ARG1
sym=ARG2
set terminal pdfcairo enh col size 16.0cm,12.0cm font "Ubuntu,12"
set output sprintf("../radiant_%s.pdf", sys)
set encoding iso

set multiplot
set lmargin 0.0
set rmargin 1.0
set tmargin 0.2
set bmargin 0.2
set grid
set xr [-180:180]
set yr [0:120]
set xtics -180,60,180 format ''
set ytics 0,30,119
set mxtics 3
set mytics 3
set tic front

set label 10 'Azimuth Angle (degree)' \
center at screen .5, screen .0 offset 0,1.5 font ',20'
set label 11 'Incident Angle (degree)' rotate by 90 \
center at screen .0, screen .5 offset 1.5,0.0 font ',20'
# set label 12 sprintf('{/Ubuntu-Bold %s}', sym) \
left at screen 0, screen 1 offset 1.5,-1.5 font ',28'
lm = 0.07
rm = 0.07
bm = 0.10
tm = 0.03
px = (1-lm-rm)/2.0
py = (1-bm-tm)/2.0
set size px,py

set palette gray negative
set cbr [0:20]
set cbtics offset -0.5,0
set colorbox user size char 3.5,screen .98-bm*1.2-tm origin screen 1-rm,bm*1.2
set label 1 sprintf('{/Symbol q} = 90{\272}') \
front left at graph 0, graph 1 offset 1,-1.3 font ',16'

set xtics format ''
set ytics format '%.0f'
set origin 1-2*px-rm, 1.0-py-tm
plot sprintf('../data/radiant_%s_90.dat', sys) binary format='%double' \
     array=(120,120) ax x2y2 with image not

unset label 10
unset label 11
unset label 12
unset colorbox

set label 1 sprintf('{/Symbol q} = 60{\272}',sym)
set xtics format ''
set ytics format ''
set origin 1.0-px-rm, 1.0-py-tm
plot sprintf('../data/radiant_%s_60.dat', sys) binary format='%double' \
     array=(120,120) ax x2y2 with image not

set label 1 sprintf('{/Symbol q} = 30{\272}')
set xtics -180,60,179 format '%.0f'
set ytics format '%.0f'
set origin 1-2*px-rm, 1-2*py-tm
plot sprintf('../data/radiant_%s_30.dat', sys) binary format='%double' \
     array=(120,120) ax x2y2 with image not

set label 1 sprintf('{/Symbol q} = 10{\272}')
set xtics -180,60,180 format '%.0f'
set ytics format ''
set origin 1.0-px-rm, 1-2*py-tm
plot sprintf('../data/radiant_%s_10.dat', sys) binary format='%double' \
     array=(120,120) ax x2y2 with image not

unset label
unset multiplot
unset output
} else {
call 'radiant_map.gp' "sys1" "A"
call 'radiant_map.gp' "sys2" "B"
call 'radiant_map.gp' "sys3" "C"
}

