#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col dl 1 size 13.0cm,9.0cm font "Ubuntu,16"
set output '/dev/null'
set encoding iso

set lmargin 8.0
set rmargin 1.5
set bmargin 0.5
set tmargin 0.0

set tic front
set bar 0
set key left Left rev font ',16'

w = 10.
set boxwidth w
bin(x,n,d) = (x<=n*d)?n*d-d/2:bin(x,n+1,d)

sign(x) = (x>0)?1:(x==0)?0:-1
prodV(x,y,z,t) = \
  (x-cos(t)**2*x-cos(t)*sin(t)*z)*(-cos(t)*sin(t)) \
    +(z-cos(t)*sin(t)*x-sin(t)**2*z)*(cos(t)**2)
absV0(t) = sqrt((cos(t)*sin(t))**2+cos(t)**4)
absVt(x,y,z,t) = \
  sqrt((x-cos(t)**2*x-cos(t)*sin(t)*z)**2+y**2\
  +(z-cos(t)*sin(t)*x-sin(t)**2*z)**2)
angle(x,y,z,t) = sign(y)*acos(prodV(x,y,z,t)/absV0(t)/absVt(x,y,z,t))/pi*180

_fn0='../data/sys2_10.dat'
_fn1='../data/sys2_30.dat'
_fn2='../data/sys2_60.dat'
_fn3='../data/sys2_90.dat'
_c0=0;_c1=0;_c2=0;_c3=0
_n0=10;_n1=30;_n2=60;_n3=90
_y0=0.24;_y1=0.24;_y2=0.14;_y3=0.07
_my0=0.1;_my1=0.1;_my2=0.05;_my3=0.02

get_fn(n) = (n==0)?_fn0:(n==1)?_fn1:(n==2)?_fn2:(n==3)?_fn3:'/dev/null'
get_c(n) = (n==0)?_c0:(n==1)?_c1:(n==2)?_c2:(n==3)?_c3:1/0
get_n(n) = (n==0)?_n0:(n==1)?_n1:(n==2)?_n2:(n==3)?_n3:1/0
get_y(n) = (n==0)?_y0:(n==1)?_y1:(n==2)?_y2:(n==3)?_y3:1/0
get_my(n) = (n==0)?_my0:(n==1)?_my1:(n==2)?_my2:(n==3)?_my3:1/0

do for [n=0:3] {
c=0;plot get_fn(n) binary format='%8double' u (c=c+1) w p not
eval sprintf('_c%d = 1.0*c', n)
}

set output '../angle_sys2.pdf'

set multiplot
set origin 0,0.11
set size 1,0.29
set xlabel 'Position Angle (deg.)' font ',20' offset 0,0.3
set label 1 'Probability Distribution' font ',20' \
center at char 1.0, screen 0.5 rotate by 90 offset 0,1
# set label 2 'B' font 'Ubuntu Bold,28' \
# left at screen 0, screen 1 offset 0.6,-0.8
set xr [-190:190]
set yr [0:get_y(1)]
set xtics -180,60,180 format '%.0f'
set ytics 0,get_my(1),1 format '%4.2f'
set mxtics 6
set mytics 5
set label 10 front sprintf('{/Symbol a} = %2d{\272}', get_n(1)) \
at graph 0, graph 1 offset 1,-.8 font ',18'
plot get_fn(1) binary format='%8double' \
     u (bin(angle($4,$5,$6,pi/180*get_n(0)),-20,w)):(1./get_c(1)) \
     s freq w boxes fs solid 0.3 lc 2 not

unset ylabel
unset xlabel
unset label 1
unset label 2
set xtics format ''
do for [n=2:3] {
set yr [0:get_y(n)]
set ytics 0,get_my(n),1 format '%4.2f'
set label 10 front sprintf('{/Symbol a} = %2d{\272}', get_n(n))
set origin 0,0.11+(n-1)*0.29
plot get_fn(n) binary format='%8double' \
     u (bin(angle($4,$5,$6,pi/180*get_n(n)),-20,w)):(1./get_c(n)) \
     s freq w boxes fs solid 0.3 lc 2 not
}

unset multiplot
###
