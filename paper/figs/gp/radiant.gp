if (!exists("n")) { n = 0 }

if (ARGC>0) {
sys=ARG1
sym=ARG2
set terminal pdfcairo enh col size 14.0cm,12.0cm font "Ubuntu,12"
set output sprintf("../radiant_%s.pdf", sys)
set encoding iso

set multiplot
set lmargin 0
set rmargin 1
set tmargin 0.5
set bmargin 0.5
set grid
set xr [-200:200]
set yr [-5:115]
set xtics -180,60,180 format ''
set ytics 0,30,120
set mxtics 3
set mytics 3

set label 10 'Azimuth Angle (degree)' \
center at screen .5, screen .0 offset 0,1.0 font ',20'
set label 11 'Incident Angle (degree)' rotate by 90 \
center at screen .0, screen .5 offset 1.5,0.0 font ',20'
lm = 0.09
bm = 0.08
px = (1-lm)/2.0
py = (1-bm)/2.0
set size px,py
set label 1 sprintf('{/Symbol q} = 90{\272}',sym) \
left at graph 0, graph 1 offset 1,-1.3 font ',16'

set xtics format ''
set ytics format '%.0f'
set origin 1-2*px, 1.0-py
plot sprintf('../data/%s_90.dat', sys) binary format='%8double' \
     u (r=sqrt($4**2+$5**2), atan2($5,$4)/pi*180):(90+atan2($6,r)/pi*180) \
     ev 5 w d lc 2 not

unset label 10
unset label 11

set label 1 sprintf('{/Symbol q} = 60{\272}',sym)
set xtics format ''
set ytics format ''
set origin 1.0-px, 1.0-py
plot sprintf('../data/%s_60.dat', sys) binary format='%8double' \
     u (r=sqrt($4**2+$5**2), atan2($5,$4)/pi*180):(90+atan2($6,r)/pi*180) \
     ev 5 w d lc 2 not

set label 1 sprintf('{/Symbol q} = 30{\272}',sym)
set xtics format '%.0f'
set ytics format '%.0f'
set origin 1-2*px, 1-2*py
plot sprintf('../data/%s_30.dat', sys) binary format='%8double' \
     u (r=sqrt($4**2+$5**2), atan2($5,$4)/pi*180):(90+atan2($6,r)/pi*180) \
     ev 5 w d lc 2 not

set label 1 sprintf('{/Symbol q} = 10{\272}',sym)
set xtics format '%.0f'
set ytics format ''
set origin 1.0-px, 1-2*py
plot sprintf('../data/%s_10.dat', sys) binary format='%8double' \
     u (r=sqrt($4**2+$5**2), atan2($5,$4)/pi*180):(90+atan2($6,r)/pi*180) \
     ev 5 w d lc 2 not

unset label
unset multiplot
unset output
} else {
call 'radiant.gp' "sys1" ""
call 'radiant.gp' "sys2" ""
call 'radiant.gp' "sys3" ""
}

