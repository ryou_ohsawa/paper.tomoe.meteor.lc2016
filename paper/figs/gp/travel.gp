#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 12.0cm,7.0cm font "Ubuntu,16"
set output '/dev/null'
set encoding iso

set xr [-80:1480]
set yr [0:1.2]
set xtics 0,200,1500 format '%.0f'
set ytics 0,0.2,1 format '%3.1f'
set mxtics 2
set mytics 5

set lmargin 7.5
set rmargin 1.5
set bmargin 3.5
set tmargin 1.0
set xlabel 'Travel Distance (km)' font ',20'
set ylabel 'Probability Distribution' font ',20'

set tic front
set bar 0
set key left Left rev inv font ',14'

c = 0.
w = 50.
set boxwidth w
bin(x,n,d) = (x<=n*d)?n*d-d/2:bin(x,n+1,d)
plot \
 '../data/sys1_60.dat' binary format='%8double' \
 u (bin($8,0,w)):(c=c+1) s cum w p not
###
set output '../travel.pdf'
plot \
  '../data/sys1_60.dat' binary format='%8double' \
  u (bin($8,0,w)-w/2):(1./c) s cum w steps \
  lc 2 t 'Cumulative Distribution' ,\
  '../data/sys1_60.dat' binary format='%8double' \
  u (bin($8,0,w)):(1./c) s freq w boxes \
  fs solid 0.3 lc 2 t "Distribution for System 1 ({/Symbol a} = 60{\272})"
###
