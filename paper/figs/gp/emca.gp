#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 14.0cm,11.0cm font "Ubuntu,16"
set output '../emca.pdf'

set xr [100:0]
set yr [0.002:200]
set xtics 0,10,99 format '%.0f'
set ytics auto format '%3g'
set mxtics 2
set mytics 10
set log y
unset grid

set lmargin 8.0
set rmargin 1.5
set bmargin 3.5
set tmargin 1.0
set xlabel 'Elevation Angle (degree)' font ',20'
set ylabel 'Effective Meteor-Collecting Area (10^3&{,}km^2)' font ',20'

eps = 1e-6
R = 6371.
H = 120.
d = pi/180*0.5
D(t,h) = sqrt((R*sin(t))**2+h**2+2*R*h)-R*sin(t)
L(t,h) = \
  sqrt((D(t-d,h)*cos(t-d)-D(t+d,h)*cos(t+d))**2+\
  (D(t-d,h)*sin(t-d)-D(t+d,h)*sin(t+d))**2)
H(t,h) = (2*h*tan(d))*(D(t,h)/D(pi/2,h))
s(t,h) = (H(t-d,h)+H(t+d,h))*L(t,h)/2.0
S(x) = s(pi/180*((x<90+eps)?(x>5-eps)?x:1/0:1/0),120)
sp(t) = (H(t-d,120)+H(t+d,80)*((R+120.)/(R+80.)))/2.0\
  * R*(asin(D(t-d,120)/(R+120)*cos(t-d))-asin(D(t+d,80)/(R+80)*cos(t+d)))
Sp(x) = max(sp(pi/180*((x<90+eps)?(x>5-eps)?x:1/0:1/0)), S(x))

w1(t) = s(t,120)
w2(t) = s(t,80)
w3(t) = (H(t-d,120)+H(t-d,80)*((R+120.)/(R+80.)))/2.0\
  * (D(t-d,120)-D(t-d,80))
w4(t) = (L(t,120)+L(t,80))/2.0 * (D(t,120)-D(t,80))
w6(t) = (H(t+d,120)+H(t+d,80)*((R+120.)/(R+80.)))/2.0\
  * (D(t+d,120)-D(t+d,80))
w(t) = w1(t)+w2(t)+w3(t)+w4(t)+w4(t)+w6(t)
W(x) = w(pi/180*((x<90+eps)?(x>5-eps)?x:1/0:1/0))

r = (100./120.)**2
set bar 0
set key left Left rev font ',20'
plot \
  for [n=1:3] sprintf('../data/emca_sys%d.tbl',n) u 1:(r*$2/1e3):(r*$3/1e3) \
  t sprintf('System %d', n) lc n+1 ps 0.7+n/10. pt 2*n+3 w yerrorline
###
#  S(x)/1e3 dt 2 lc rgb 'gray30' t 'Projected FOV of System 1', \
#  Sp(x)/1e3 dt 4 lc rgb 'gray30' t 'Projected MCV of System 1', \
#  '../dat/emca_sys5.tbl' u 1:(r*$2/1e3) w l not lc 3 dt 2

