#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 14.5cm,4.3cm font "Ubuntu,12"
set output "../tomoepm_collectarea.pdf"
set encoding iso
unset border
unset xtics
unset ytics
unset ylabel
set tmargin .0
set lmargin .0
set rmargin .0
set bmargin .0
set size 1.05
set size ratio -1
set origin -0.025, -0.025

R=6371.
H1=80.
H2=120.
set yr [6230:6680]
set xr [-350:1150]
set parametric
set tr [-pi:pi]
set sample 1e3

ZD=75.
FOV=2.

s(t) = sin(t*pi/180.)
c(t) = cos(t*pi/180.)
Q(Z) = R*c(Z)
L(Z,H) = sqrt(Q(Z)**2+H**2+2*R*H) - Q(Z)
Lx(Z,H) = L(Z,H)*s(Z)
Ly(Z,H) = L(Z,H)*c(Z)
T0(Z,H) = acos((R**2+(R+H)**2-L(Z,H)**2)/(2*R*(R+H)))

set style arrow 1 front nohead dt 3 lw 1 lc rgb 'gray30'
set style arrow 2 front nohead dt 4 lw 1 lc rgb 'gray30'
set style arrow 3 front backhead \
size 15,15 fill lw 1.5 lc rgb 'coral'

set arrow 1 from 0,R rto Lx(ZD-FOV,H1),Ly(ZD-FOV,H1) as 1
set arrow 2 from 0,R rto Lx(ZD+FOV,H1),Ly(ZD+FOV,H1) as 1
set arrow 3 from 0,R rto 180,0 as 2

set object 1 circle front at 0,R size 5 \
fs solid 1 border lc rgb 'royalblue' fc rgb 'royalblue'

set object 2 polygon front \
from 0+Lx(ZD-FOV,H1),R+Ly(ZD-FOV,H1) \
to   0+Lx(ZD-FOV,H2),R+Ly(ZD-FOV,H2) \
to   0+Lx(ZD+FOV,H2),R+Ly(ZD+FOV,H2) \
to   0+Lx(ZD+FOV,H1),R+Ly(ZD+FOV,H1) \
to   0+Lx(ZD-FOV,H1),R+Ly(ZD-FOV,H1) \
fs trans pattern 2 border lc rgb 'royalblue' fc rgb 'royalblue'

set label 1 'Earth Surface' front \
right at R*s(10.), R*c(10.) rotate by -7.4 \
font ',14' offset 0, 0.3 tc rgb hsv(150,95,80)
set label 2 'Telescope' front center at 0, R \
font ',14' offset 0,-0.8 tc rgb hsv(150,95,80)
set label 3 '{/Symbol a}' front \
font ',12' center at 180,R tc rgb 'gray30' \
offset -.2,.8 rotate by -8
set label 4 'Test Region' front center at 0, R+H2 \
font ',10' offset 0,0.3 tc rgb 'royalblue'

set arrow 10 from 0+Lx(ZD,H2),R+Ly(ZD,H2) \
rto (0+Lx(ZD,H2))/90.,(R+Ly(ZD,H2))/90. as 2
set arrow 11 from 0+Lx(ZD,H2),R+Ly(ZD,H2) \
rto 120*s(30),120*c(30) as 3

set label 11 'incident meteor' front \
center at 0+Lx(ZD,H2)+80*s(30),R+Ly(ZD,H2)+80*c(30) \
font ',10' tc rgb 'coral' offset 2.0,0.6 rotate by 60
set label 12 '{i}' front \
center at 0+Lx(ZD,H2)+(0+Lx(ZD,H2))/90.,R+Ly(ZD,H2)+(R+Ly(ZD,H2))/90. \
font ',12' tc rgb 'gray30' offset 1,0 rotate by -8

set label 21 sprintf('H_1 = %.0fkm',H1) front \
right at (R+H1-5)*s(9.8), (R+H1-5)*c(9.8) rotate by -7.3 \
font ',10' tc rgb 'gray30'
set label 22 sprintf('H_2 = %.0fkm',H2) front \
right at (R+H2+10)*s(9.8), (R+H2+10)*c(9.8) rotate by -7.3 \
font ',10' tc rgb 'gray30'

plot \
     0+Lx(ZD,H2)-60*sin(t/16-0.29), \
     R+Ly(ZD,H2)+60*cos(t/16-0.29) \
     w l not lc rgb 'gray30', \
     (R+H2)*sin(t),(R+H2)*cos(t) w filledc above r=R \
     fs solid 0.1 noborder lc rgb 'royalblue' not,\
     (R+H2)*sin(T0(ZD,0.5*(H1+H2))+t*(10/360.)),\
     (R+H2)*cos(T0(ZD,0.5*(H1+H2))+t*(10/360.)) \
     w filledc above r=R \
     fs solid 0.4 noborder lc rgb 'royalblue' not,\
     (R+H1)*sin(t),(R+H1)*cos(t) w filledc closed \
     fs solid 1.0 noborder lc rgb 'white' not, \
     R*sin(t),R*cos(t) w filledc closed \
     fs solid 0.4 noborder lc rgb hsv(150,95,180) not, \
     0+160*cos((t+pi)*(90-ZD)/360), \
     R+160*sin((t+pi)*(90-ZD)/360) \
     w l not lc rgb 'gray30'
###
unset output
