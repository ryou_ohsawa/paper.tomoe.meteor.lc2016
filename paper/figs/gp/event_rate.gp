#!/usr/bin/gnuplot
set terminal pdfcairo size 19cm,10.5cm enh \
  font 'Ubuntu, 16' dashed background 'white'
set encoding iso

set lmargin 7
set rmargin 2
set boxwidth 1
set key samplen 1 font ',22'
set size 1
set origin 0,0
set timefmt '%H:%M'
set xdata time

## 20160411
set output '../event_rate_20160411.pdf'
F='../data/event_rate_zmag_20160411.tbl'
Z='../data/event_rate_zmag_20160411.tbl'

set multiplot
set tmargin 2
set key tmargin right
set bmargin 0
set tics front
set ytics 0,5,30 format '%3.0f' font ',18' offset 0.5,0.3
set mytics 1
set ylabel 'Number of Events' font 'Ubuntu Bold,24'
set xlabel ''
set mxtics 2
set xtics auto timedate format ''
set xrange ['11:45':'19:15']
set yrange [0:30]
set size 1,0.7
set origin 0,0.3
set boxwidth 180

set object 1 rect from '12:19:30',graph 0 to '12:31:00',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'
set object 2 rect from '13:03:00',graph 0 to '13:07:00',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'

set object 3 rect from '14:13:00',graph 0 to '14:18:00',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'
set object 4 rect from '16:30:00',graph 0 to '17:08:00',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'

plot \
  F u 2:4 w boxes fc rgb 'coral' fs solid noborder \
  t 'Meteor Events (8 chips,180s) on 2016-04-11'

set size 1,0.3
set origin 0,0.0
set ytics 20,2,27 format '%3.0f' font ',16' offset 0.5,0
set ylabel 'zero mag.' font 'Ubuntu Bold,24'
set yrange [22:26.8]
set mytics 5
set xtics timedate format '%H:%M' font ',18' offset 0,0.5
set xlabel 'Observation Time (UT)' font 'Ubuntu Bold,24' offset 0,.8
set tmargin 0
set bmargin 2.5

plot Z u 2:5 w p pt 7 ps 0.5 lc rgb 'coral' not

unset multiplot
unset output
unset object

## 20160414
set output '../event_rate_20160414.pdf'
F='../data/event_rate_zmag_20160414.tbl'
Z='../data/event_rate_zmag_20160414.tbl'

set multiplot
set tmargin 2
set bmargin 0
set tics front
set ytics 0,5,30 format '%3.0f' font ',18' offset 0.5,0.3
set mytics 1
set ylabel 'Number of Events' font 'Ubuntu Bold,24'
set xlabel ''
set mxtics 2
set xtics timedate format ''
set xrange ['11:45':'19:15']
set yrange [0:22]
set size 1,0.7
set origin 0,0.3

set object 1 rect from '15:00:00',graph 0 to '16:03:00',graph 1 \
fs pattern 6 noborder fc rgb 'dark-green'
set object 2 rect from '16:50:00',graph 0 to '17:30:00',graph 1 \
fs pattern 6 noborder fc rgb 'dark-green'

set object 3 rect from '14:14:00',graph 0 to '14:19:10',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'
set object 4 rect from '16:30:50',graph 0 to '16:36:10',graph 1 \
fs pattern 2 noborder fc rgb 'gray20'

plot \
  F u 2:4 w boxes fc rgb 'royalblue' fs solid noborder \
  t 'Meteor Events (8 chips,180s) on 2016-04-14'

set size 1,0.3
set origin 0,0.0
set ytics 20,2,27 format '%3.0f' font ',16' offset 0.5,0
set ylabel 'zero mag.' font 'Ubuntu Bold,24'
set yrange [22:26.8]
set mytics 5
set xtics format '%3.0f' font ',18' offset 0,0.5
set xlabel 'Observing Time (UT)' font 'Ubuntu Bold,24' offset 0,.8
set tmargin 0
set bmargin 2.5

plot Z u 2:5 w p pt 7 ps 0.5 lc rgb 'royalblue' not

unset multiplot
unset output
