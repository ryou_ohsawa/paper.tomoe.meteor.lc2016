#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 18.0cm,9.5cm font "Ubuntu,16"
set output '../systems.pdf'
set encoding iso

set size ratio -1
set xr [-300:900]
set yr [-350:250]
set xtics -1000,60,1000 format ''
set ytics -1000,60,1000 format ''
set grid lc rgb 'gray60'

set lmargin 0
set rmargin 0
set bmargin 1
set tmargin 1
unset xlabel
unset ylabel
#set xlabel 'Dimension (arcmin)' font ',20'
#set ylabel 'Dimension (arcmin)' font ',20'

set arrow 1 head fill \
from 120,120 to 120,180 lc rgb 'gray20'
set label 1 'zenith' center at 120,180 \
offset 0,0.5 tc rgb 'gray20' font 'Ubuntu Bold,14'

set arrow 2 nohead \
from -250,-280 to -250+120,-280 lw 2 lc rgb 'gray20'
set label 2 '120 arcmin' center at -250+60,-280 \
offset 0,0.7 tc rgb 'gray20' font 'Ubuntu Bold,14'

set key left Left rev font ',20'
plot \
  '../data/system1.tbl' u 1:2 \
  w filledc closed lc 1 fs pattern 2 border \
  t "System 1", \
  '../data/system2.tbl' i 0 u 1:($2-160) \
  w filledc closed lc 2 fs pattern 8 border \
  t "System 2", \
  for [n=1:7] '../data/system2.tbl' i n u 1:($2-160) \
  w filledc closed lc 2 fs pattern 8 border not, \
  '../data/system3.tbl' i 0 u ($1+600):($2-50) \
  w filledc closed lc 3 fs solid 0.5 border \
  t "System 3", \
  for [n=1:83] '../data/system3.tbl' i n u ($1+600):($2-50) \
  w filledc closed lc 3 fs solid 0.5 border not
###
###
#  '../data/system2p.tbl' u 1:($2-240) \
#  w filledc closed lc 2 dt 4 fs pattern 8 border \
#  t "System 2{\264}", \
