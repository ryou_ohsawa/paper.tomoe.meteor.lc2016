#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col size 14.0cm,9.0cm font "Ubuntu,14"
set output '../rmca.pdf'
set encoding iso

set xr [100:0]
set yr [0.02:12]
set xtics 0,10,99 format '%.0f'
set ytics auto format '%3g'
set mxtics 2
set mytics 10
set log y
unset grid

set lmargin 8.0
set rmargin 1.5
set bmargin 3.5
set tmargin 1.5
set xlabel 'Elevation Angle (degree)' font ',18'
set ylabel 'Reduced Meteor-Collecting Area (km^2)' font ',18' offset 0.5,0.0
set label 10 '({\327}10^3)' right at graph 0, graph 1 offset 1.2,0.8

set label 2 'System 2' at 98,1 offset 0,-3.1 \
tc rgb rgb(86, 180, 233) font 'Ubuntu Bold,14'
set label 4 'EMCA for System 2' left at 25,2 offset -2,-1.3 \
tc rgb 'gray80' font 'Ubuntu Bold,12' rotate by 30

r = (100./120.)**2
set bar 0
set key left Left rev font ',14'
set key at graph 0.01, graph 0.96 samplen 1
plot \
  '../data/emca_sys2.tbl' u 1:(r*$2/1e3) w l lc rgb 'gray80' dt 2 lw 2 not, \
  for [n=0:4] 1e-6 w p lc 0 pt n+4 ps 1.2+n/14. t sprintf('r = %.1f',2+0.5*n),\
  for [n=0:4] sprintf('../data/rmca_sys2_%.1f.tbl',2+0.5*n) \
  u 1:(r*$2/1e3):(r*$3/1e3) lc 3 pt n+4 ps 0.6+n/20. dt n+1 w yerrorline not
###

