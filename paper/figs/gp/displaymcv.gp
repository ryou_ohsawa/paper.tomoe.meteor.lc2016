if (!exists("n")) { n = 0 }

if (ARGC>0) {
fname=ARG1
plot_title=ARG2
set terminal pdfcairo enh col size 14.0cm,6.0cm font "Ubuntu,12"
set output sprintf("../mcv_%s.pdf", fname)

set multiplot
set lmargin 8
set rmargin 2
set tmargin 1
set bmargin 1
set grid
Re=6371
set object 1 circle behind at 0,0 size 2 \
fc rgb "royalblue" fs solid 1 noborder

set label 10 plot_title center at screen 0.5, screen 1.0 \
offset 0,-1.6 font "Ubuntu,16"

set xr [-50:250]
set yr [-45:45]
set size 1,0.38
set origin 0,0.52
set tmargin 0.5
set bmargin 0.5
set xtics -200,100,800 format ""
set ytics -100,20,100 format "%4.0f"
set mxtics 2
set mytics 5
unset xlabel
set ylabel "Distance Y (km)" font "Ubuntu,14"
set label 1 "A. top view" at graph 0, graph 1 \
offset 1,-1 font "Ubuntu Bold,12"
plot \
   for [n=1:3] sprintf("../data/mcv_%s_%2d.tbl",fname, 10+25*n) \
   u 1:2 w l lc 1+n not, \
   for [n=1:3] sprintf("../data/mcv_%s_%2d.tbl",fname, 10+25*n) \
   u 1:(-$2):(sprintf("%c", 68-n)) i 0 ev ::2:2:2:2 w labels \
   center offset 0.0,0.5 rotate by 0. tc rgb 'gray30' not

unset label 10
set yr [-30:200]
set size 1,0.5
set origin 0,0.03
set tmargin 0
set bmargin 2
set xtics 0,100,800 format "%3.0f"
set ytics -100,50,199 format "%4.0f"
set xlabel "Distance X (km)" font "Ubuntu,14" offset 0,0.6
set ylabel "Altitude (km)" font "Ubuntu,14"
set label 1 "B. side view" at graph 0, graph 1 \
offset 1,-1 font "Ubuntu Bold,12"
set label 10 "H_1" at 0,80 offset -3,-.5 \
font "Ubuntu Bold,10" tc rgb 'gray30'
set label 11 "H_2" at 0,120 offset -3,.5 \
font "Ubuntu Bold,10" tc rgb 'gray30'

set object 1 front
plot \
   sqrt(Re**2-x**2)-Re w l lc 2 lw 2 not, \
   sqrt((Re+80)**2-x**2)-Re w l lc rgb 'gray60' dt 2 lw 1 not, \
   sqrt((Re+120)**2-x**2)-Re w l lc rgb 'gray60' dt 2 lw 1 not, \
   for [n=1:3] sprintf("../data/mcv_%s_%2d.tbl",fname, 10+25*n) \
   u 1:3 w l lc 1+n not

unset label
unset multiplot
unset output
} else {
call 'displaymcv.gp' "sys1" "System 1"
call 'displaymcv.gp' "sys2" "System 2"
call 'displaymcv.gp' "sys3" "System 3"
}

