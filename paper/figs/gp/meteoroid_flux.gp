#!/usr/bin/gnuplot
set terminal pdfcairo enh col dl 1 size 16.0cm,10.0cm font "Ubuntu,16"
set encoding iso

set output "../meteoroid_flux.pdf"

logL(M,M1,M2,r,logN0) = (M>M1 && M<M2)?(logN0 + M*log10(r)):1/0

set key left rev Left samplen 5
set sample 200

set rmargin 1.0
set lmargin 6.5
set tmargin 0.5
set bmargin 3.2

set xr [-7:13]
set xtics -10,2,16
set mxtics 2
set xlabel "Meteor Absolute Magnitude"

set yr [-7.5:2.5]
set ytics -10,1,10 format "%2.0f"
set mytics 2
set ylabel "Log Cumulative Meteor Rate (km^{-2}&{,}hour^{-1})" offset -0.5,0


plot \
  "+" u 1:(y=logL($1,4,10,3.1,-5.5),y+0.2):(y-0.2) w filledc \
  lw 0 lc 6 fs solid 0.5 t "Present Result", \
  logL(x,-2.0, +4.0, 10**0.538,-5.17) \
  lc 1 lw 1.0 dt 1 t "Photographic (Hawkins \\& Upton, 1958)", \
  logL(x,+8.0,+12.0, 10**0.533,13.556-17.88-0.533*0.0) \
  lc 2 lw 1.5 dt '-' t "Phototube (Cook et al., 1980)", \
  logL(x,+2.8, +7.8, 10**0.534,13.556-17.89-0.534*0.0) \
  lc 3 lw 1.5 dt '-.' t "Television (Clifton, 1973)", \
  logL(x,+3.0, +6.0, 2.5, 13.556-18.24) \
  lc 4 lw 1.5 dt '-..' t "Television (Hawkes \\& Jones, 1975)", \
  logL(x,-10.0,+1.0, 2.5, 13.556-18.10) \
  lc 7 lw 1.5 dt '-...' t "Visual (Hawkins, 1959)"
