#!/usr/bin/gnuplot
set terminal pdfcairo size 16cm,6.6cm enh \
  font "Ubuntu, 16" dashed background 'white'
set encoding iso

set size 1
set origin 0,0
set tics front

set lmargin 7.0
set rmargin 2
set tmargin 1.5
set bmargin 2.8

set boxwidth 1
set key top right samplen 2 font ',18'

set xrange [2.4:4.0]
set yrange [0:13.5]

set ytics 0,2,100 format ' %2.0f' font ',18'
set mytics 4
set ylabel 'Frequency' font 'Ubuntu,22' offset 0.5,0
set xtics format '%.1f' font ',18' offset 0,.3
set xlabel 'Slope Parameter (r)' font 'Ubuntu,22' offset 0,.5
set mxtics 5

set label 99 '({\327}10^{-2})' right at graph 0, graph 1 \
offset 1.6,0.8 font ',16'

set style arrow 1 front nohead dt 2 lw 2 lc rgb 'coral'
set style arrow 2 front nohead dt 4 lw 2 lc rgb 'royalblue'
set style arrow 3 front nohead dt 3 lw 3 lc rgb 'gray60'

r11=3.24; r14=3.08;
set arrow 1 from r11,0 to r11,13.5 as 1
set arrow 2 from r14,0 to r14,13.5 as 2
set label 1 sprintf('{/Ubuntu-Bold=14 %.2f}',r11) front \
right at r11,13.5 rotate by 90 offset 0.7,-0.3 tc 'coral'
set label 2 sprintf('{/Ubuntu-Bold=14 %.2f}',r14) front \
right at r14,13.5 rotate by 90 offset 0.7,-0.3 tc 'royalblue'

## 20160411+20160414
set output '../model_fitting_r.pdf'

bins(x,n,d) = (x<n*d)?(n*d-d/2):(bins(x,n+1,d))
n0 = 2*50
d0 = 1/50.
set boxwidth d0

plot \
  '<gunzip --stdout ../data/simple.r.20160411.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'coral' \
  t '2016-04-11', \
  '<gunzip --stdout ../data/simple.r.20160414.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'royalblue' \
  t '2016-04-14'
###

unset output

set xrange [-6.3:-4.5]

set ytics 0,2,100 format ' %2.0f' font ',18'
set mytics 4
set ylabel 'Frequency' font 'Ubuntu,22' offset 0.5,0
set xtics format '%.1f' font ',18' offset 0,.3
set xlabel '{Log Meteor Rate} (log_{10}&{,}N_0)' font ',22' offset 0,.5
set mxtics 5

set label 99 '({\327}10^{-2})' right at graph 0, graph 1 \
offset 1.6,0.8 font ',16'

N11=-5.59; N14=-5.34;
set arrow 1 from N11,0 to N11,13.5 as 1
set arrow 2 from N14,0 to N14,13.5 as 2
set label 1 sprintf('{/Ubuntu-Bold=14 %.2f}',N11) front \
right at N11,13.5 rotate by 90 offset .7,-0.3 tc 'coral'
set label 2 sprintf('{/Ubuntu-Bold=14 %.2f}',N14) front \
right at N14,13.5 rotate by 90 offset .7,-0.3 tc 'royalblue'

## 20160411+20160414
set output '../model_fitting_log10_N0.pdf'

bins(x,n,d) = (x<n*d)?(n*d-d/2):(bins(x,n+1,d))
n0 = -8*50
d0 = 1/50.
set boxwidth d0

plot \
  '<gunzip --stdout ../data/simple.log10_N0.20160411.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'coral' \
  t '2016-04-11', \
  '<gunzip --stdout ../data/simple.log10_N0.20160414.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'royalblue' \
  t '2016-04-14'
###

unset output

set xrange [2.4:4.0]
set yrange [0:13.5]

set ytics 0,2,100 format ' %2.0f' font ',18'
set mytics 4
set ylabel 'Frequency' font 'Ubuntu,22' offset 0.5,0
set xtics format '%.1f' font ',18' offset 0,.3
set xlabel 'Slope Parameter (r)' font 'Ubuntu,22' offset 0,.5
set mxtics 5

set label 99 '({\327}10^{-2})' right at graph 0, graph 1 \
offset 1.6,0.8 font ',16'

r11=3.24; r14=3.07;
set arrow 1 from r11,0 to r11,13.5 as 1
set arrow 2 from r14,0 to r14,13.5 as 2
set label 1 sprintf('{/Ubuntu-Bold=14 %.2f}',r11) front \
right at r11,13.5 rotate by 90 offset 0.7,-0.3 tc 'coral'
set label 2 sprintf('{/Ubuntu-Bold=14 %.2f}',r14) front \
right at r14,13.5 rotate by 90 offset 0.7,-0.3 tc 'royalblue'

## 20160411+20160414
set output '../model_fitting_r_diurnal.pdf'

bins(x,n,d) = (x<n*d)?(n*d-d/2):(bins(x,n+1,d))
n0 = 2*50
d0 = 1/50.
set boxwidth d0

plot \
  '<gunzip --stdout ../data/diurnal.r.20160411.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'coral' \
  t '2016-04-11', \
  '<gunzip --stdout ../data/diurnal.r.20160414.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'royalblue' \
  t '2016-04-14'
###

unset output

set xrange [-6.3:-4.5]

set ytics 0,2,100 format ' %2.0f' font ',18'
set mytics 4
set ylabel 'Frequency' font 'Ubuntu,22' offset 0.5,0
set xtics format '%.1f' font ',18' offset 0,.3
set xlabel '{Log Meteor Rate} (log_{10}&{,}N_0)' font ',22' offset 0,.5
set mxtics 5

set label 99 '({\327}10^{-2})' right at graph 0, graph 1 \
offset 1.6,0.8 font ',16'

N11=-5.59; N14=-5.34;
set arrow 1 from N11,0 to N11,13.5 as 1
set arrow 2 from N14,0 to N14,13.5 as 2
set label 1 sprintf('{/Ubuntu-Bold=14 %.2f}',N11) front \
right at N11,13.5 rotate by 90 offset .7,-0.3 tc 'coral'
set label 2 sprintf('{/Ubuntu-Bold=14 %.2f}',N14) front \
right at N14,13.5 rotate by 90 offset .7,-0.3 tc 'royalblue'

## 20160411+20160414
set output '../model_fitting_log10_N0_diurnal.pdf'

bins(x,n,d) = (x<n*d)?(n*d-d/2):(bins(x,n+1,d))
n0 = -8*50
d0 = 1/50.
set boxwidth d0

plot \
  '<gunzip --stdout ../data/diurnal.log10_N0.20160411.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'coral' \
  t '2016-04-11', \
  '<gunzip --stdout ../data/diurnal.log10_N0.20160414.txt.gz | sort -g' \
  u (bins($1,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'royalblue' \
  t '2016-04-14'
###

unset output

set xrange [-4.5:5.5]

set ytics 0,2,100 format ' %2.0f' font ',18'
set mytics 4
set ylabel 'Frequency' font 'Ubuntu,22' offset 0.5,0
set xtics -5,1,10 format '%.1f' font ',18' offset 0,.3
set xlabel '{Diurnal Parameter} (η)' font ',22' offset 0,.5
set mxtics 5

set label 99 '({\327}10^{-2})' right at graph 0, graph 1 \
offset 1.6,0.8 font ',16'
set label 100 '({\327}10^{-2})' right at graph 1, graph 0 \
offset 1.6,-1.8 font ',16'

p11=1.1585; p14=0.49674
set arrow 1 from p11,0 to p11,13.5 as 1
set arrow 2 from p14,0 to p14,13.5 as 2
set label 1 sprintf('{/Ubuntu-Bold=14 %.4f}',p11/100) front \
right at p11,13.5 rotate by 90 offset .7,-0.3 tc 'coral'
set label 2 sprintf('{/Ubuntu-Bold=14 %.4f}',p14/100) front \
right at p14,13.5 rotate by 90 offset .7,-0.3 tc 'royalblue'

set output '../model_fitting_eta_diurnal.pdf'

bins(x,n,d) = (x<n*d)?(n*d-d/2):(bins(x,n+1,d))
n0 = -4*10
d0 = 1/10.
set boxwidth d0

plot \
  '<gunzip --stdout ../data/diurnal.eta.20160411.txt.gz | sort -g' \
  u (bins($1*100,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'coral' \
  t '2016-04-11', \
  '<gunzip --stdout ../data/diurnal.eta.20160414.txt.gz | sort -g' \
  u (bins($1*100,n0,d0)):(100/12000.) s freq w step lw 2 lc rgb 'royalblue' \
  t '2016-04-14'
###

unset output
