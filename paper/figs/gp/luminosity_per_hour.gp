#!/usr/bin/gnuplot
set terminal pdfcairo size 16cm,21cm enh \
  font "Ubuntu, 14" dashed background 'white'
set encoding iso

set size 1
set origin 0,0
set tics front

set lmargin 4.0
set rmargin 2.0
set tmargin 1.8
set bmargin 1.5

set xrange [1.5:12.5]
set yrange [5e-5:1e-0]
set log y

set ylabel 'Cummulative Event Rates (km^{/=12 -2}&{,}hour^{/=12 -1})' font ',20'
set xlabel 'Magnitude (at 100km, V-band)' font 'Ubuntu,20' offset 0,.8
set grid x y

## 20160411+20160414
set output '../luminosity_per_hour.pdf'
F='../data/cumulative_per_hour_modified_20160411.sav'
G='../data/cumulative_per_hour_modified_20160414.sav'

set size 1
set origin 0,0
set multiplot

unset xtics
unset ytics
unset border

plot -1 not

set xtics -1,1,15 format '%.0f' font ',14' offset 0,0.4
set ytics 1e-6,10,0.9 format '10^{%-3.1L}' font ',14'
set mxtics 4
set mytics 10
set border

set key tmargin right horizontal samplen 2 font ',16'
unset xlabel
unset ylabel

n2time(n) = 11.0+n*1.25
n2label(n) = sprintf('UT=%02d:%02d-%02d:%02d', \
floor(n2time(n)), 60*(n2time(n)-floor(n2time(n))), \
floor(n2time(n+1)), 60*(n2time(n+1)-floor(n2time(n+1))) )

logN0=-5.1
SKIP=0.152
set size 0.95,0.205
set origin 0.06,0.04
unset key
set label 1 n2label(5-0) at graph 0, graph 1 \
offset 1,-1 font 'Ubuntu,14'
plot \
   3.3**x*10**logN0 w l dt 2 lc rgb 'gray10' not, \
   F i 5-0 u 1:($2) w lp pt 6 ps .4 lw 2 lc rgb 'coral' \
   t '2016-04-11', \
   G i 5-0 u 1:($2) w lp pt 8 ps .4 lw 2 lc rgb 'royalblue' \
   t '2016-04-14'

do for [idx=1:4] {
set xtics format ''
set origin 0.06,0.04+idx*SKIP
set label 1 n2label(5-idx) at graph 0, graph 1 \
offset 1,-1 font 'Ubuntu,14'
plot \
   3.3**x*10**logN0 w l dt 2 lc rgb 'gray10' not, \
   F i 5-idx u 1:($2) w lp pt 6 ps .4 lw 2 lc rgb 'coral' \
   t '2016-04-11', \
   G i 5-idx u 1:($2) w lp pt 8 ps .4 lw 2 lc rgb 'royalblue' \
   t '2016-04-14'
###
#   H u 1:($2/5.1) w l lw 2 lc rgb 'gray60' not, \
set xtics format ''
}

set key
set origin 0.06,0.04+5*SKIP
set label 1 n2label(5-5) at graph 0, graph 1 \
offset 1,-1 font 'Ubuntu,14'
plot \
   3.3**x*10**logN0 w l dt 2 lc rgb 'gray10' not, \
   F i 5-5 u 1:($2) w lp pt 6 ps .4 lw 2 lc rgb 'coral' \
   t '2016-04-11', \
   G i 5-5 u 1:($2) w lp pt 8 ps .4 lw 2 lc rgb 'royalblue' \
   t '2016-04-14'

unset multiplot
unset output
