Response to the referees' comments for:
 "Imaging Observations of Faint Sporadic Meteors with a Wide-Field CMOS mosaic camera Tomo-e PM"
by Ohsawa, et al.

---------------------


We express deep gratitude to the anonymous referees for carefully reading our manuscript and giving the fruitful comments. We generally agree with the comments from the editor and the referees. We have revised the manuscript according to the comments. The revised parts are written in red. The response to each comment is described below.



## Editor
>  In astronomy, IDP is an acronym designated for cosmic dust collected in the stratosphere. Therefore, the current manuscript contains somewhat misleading usage of IDP(s).
We have revised the manuscript as suggested.


## Referee 1
> section 1, paragraph 3 I do not believe a 10^-6 gram meteor is visible from space (magnitude +14 from the ground). Do you have a units error or exponential value error here.
We agree that a 10^-6 gram meteor is not visible even from space. We meant that satellites and spacecrafts detected 10^-6 or smaller particles with dust counters. We have revised the misleading expressions.


> section 1, paragraph 4 Suggest replacing "lose in part their kinetic energy in a form of emission" with "convert part of their kinetic energy into emission lines"
We have revised the manuscript as suggested.


> section 1, 2 paragraphs from the end. There were liquid mirror telescope collections made during the Leonids 1999 by Pawlowski and Hebert (Earth, Moon, and Planets 82-83, 249-256, 2000)
We have revised the paragraph in question. We properly refer the paper by Pawlowsky and Hebert (2000) and describe other meteor studies with large telescopes in detail.


> section 3.1, paragraph 2. "The detected meteors are thought to be sporadic meteors." You should line up these tracks with active shower radiants to weed out any contaminating meteor streams.
We have calculated possible contributions from April Lyrids and Eta Aquariids, which are most significant showers in that season. No significant excess from the background level of sporadic meteors has been detected. Thus, we have concluded that the contribution form meteor showers was negligible. The paragraph in question has been revised accordingly.


> Section 4.3 The paragraph.... "The EMCAs are almost proportional to the dimensions of the field-of-views, rather than the areas of the field-of-views. This suggests that the meteor-collecting efficiency depends on the surface area of the MCV, not the volume of the MCV. This in part explains the reason that the EMCA of System 3 is only twice larger than that of System 2."... makes no sense. One sentence states the "dimensions" rather than the "areas" define the EMCA. The next states the surface area and not the volume defines the EMCA. This needs to be rewritten and clarified. What does "dimensions" mean ? I do not believe the EMCA of system 3 is only 2x that of system 2. It should be much higher. Something is very wrong with the EMCA calculation for the 84 camera system.
The "dimensions" meant a typical length of a field-of-view --- a side length, a diagonal length, or a circumference length. We have replace the word "dimension" with appropriate one (c.f., side length) and revised the paragraph to clearly explain the dependence of the EMCA on the side length.

We are confident that the EMCA of System 3 is correct. Since most meteors are first detected in circumference detectors, the detectors in the central part of System 3 have little contribution to the meteor-collecting efficiency. For narrow field-of-view systems, the summation of the circumference sides is roughly proportional to the EMCA. The summations of the most outer circumference sides of Systems 2 and 3 are 11.3 and 19.5 degree, respectively. It seems reasonable that the EMCA of System 3 is about two times larger than that of System 2. We have revised the manuscript to make the explanation more clear.


> Section 4.3 Figure 9 is deceiving as it indicates that going lower in elevation angle improves your detection efficiency. This is only true when you are above the effects of the atmosphere (such as flying in an aircraft - see Leonid MAC mission papers). You are leaving out the effects of distance losses and atmospheric extinction losses at very low elevations. Counteracting those two effects to a lesser extent, is the gain obtained because meteors at low elevation have less apparent angular velocity loss w(rad/sec) = Vinf(km/sec) * sin(Dradiant distance) / range (km) such that range is a function of meteor height and sin(elevation angle). This needs to be included in the discussion for completeness....Ah, I see this is in the appendix A.4 - this should be referred to back in the main body of the paper - along with the other appendices subsections, which should also be referenced back in the main paper.
We have revised the manuscript so that the appendices are properly referenced in the main body.


> conclusions...Figure 6 shows at least a 10x difference with more recent observations from video systems. And the statement "roughly consistent" ignores this large discrepancy - the amount of disagreement should try to be explained in the conclusions.
We consider that the discrepancy is partly attributed to the seasonal variation in the meteor rate and different assumptions on the EMCAs. We have revised the manuscript to make it clear.


## Referee 2
> 1) In general the title, abstract and conclusions should all be consistent with one another. In the current version of the manuscript, the title focuses on the observations, the abstract discusses the luminosity function validation and the conclusion focuses on the new instrumental capability. As it is written, it is not clear what the main point of the paper is. The authors need to decide what the main points are and be consistent throughout the paper. The abstract discusses validating the luminosity function, which in itself may not be a strong enough reason to publish the results. Perhaps this could be worded as providing a new measurement of the luminosity function over a slightly different mass range?
The title has been modified accordingly. We have revised Abstract and Conclusion to be consistent throughout the paper.


> 2) Abstract: Just to clarify, when it says: "1.98 sq-degree", does that mean that the area is 1.98 (deg)² is it an area that is 1.98° by 1.98° ? can this be made more clear?
We used that expression for 1.98 deg². We have replaced "sq-degree" with "deg²".


> 3) Page 2, bottom: ...have been detected...
We have revised the manuscript as suggested.


> 4) Page 4, bottom: same comment as the above #2 relating to the area for the 20 sq-degree.
We have revised the manuscript in the same manner in the comment#2.


> 5) Page 4, bottom: can you clarify the units of velocity, you are listing an angular velocity, please specify and can you also provide a nominal velocity (assuming 100 km altitude) that is in km/s.
We have revised the manuscript to show that that is an angular velocity. We have add a sentence to show the reason that we assumed the angular velocity of 10 deg/s.


> 6) Page 5, top: 1″.19 should be 1.19″
We have revised the manuscript as suggested.


> 7) Page 5, middle:  "...suffered..." should be "...affected by..."
We have revised the manuscript as suggested.


> 8) Page 8, middle: again be clear that this is an angular velocity.
We have revised the manuscript as suggested.


> 9) Page 19: "twice larger" should be "two times larger"
We have revised the manuscript as suggested.


> 10) Page 21: can you also add the mass range in mg, i.e. (0.08― 33 mg).
We understand that the mass range in mg is widely used. We, however, used grams to describe the mass ranges in Introduction. We would like to use the same units for the mass ranges.


> 11) Page 21: last line: This is too subjective, please change this to something like:  "...has the capability to make it a leading..."
We revised the manuscript as suggested.


> 12) In general, throughout the manuscript can you be clear about the fields of view. It should be written  "fields of view", not  "field of views".
We revised the manuscript as suggested.
