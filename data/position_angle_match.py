#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division
from argparse import ArgumentParser as ap
import astropy.time as time
import astropy.units as u
import astropy.coordinates as coo
import numpy as np
import pandas as pd
import warnings


apr_lyrids = \
  coo.SkyCoord(ra=271.4*u.degree, dec=33.6*u.degree, frame='icrs')
eta_aquariids = \
  coo.SkyCoord(ra=338.0*u.degree, dec=-1.0*u.degree, frame='icrs')


def convert_radiant(se):
  xyz = np.cos(se),0,np.sin(se)
  return np.array(xyz)


def generate_great_circle(pa):
  return lambda t: \
    np.array((np.cos(t),np.sin(t)*np.sin(pa),np.sin(t)*np.cos(pa)))


def minarc(se, pa):
  se, pa = se/180.0*np.pi, pa/180.0*np.pi
  A, B = np.cos(se), np.sin(se)*np.cos(pa)
  t = np.arctan2(B,A)
  x1 = convert_radiant(se)
  x2 = generate_great_circle(pa)(t)
  return np.arccos(np.dot(x1,x2))/np.pi*180


if __name__ == '__main__':
  parser = ap('calculate position angle of meteor showars')
  parser.add_argument('meteors', help='unique meteor file')
  parser.add_argument('database', help='position angle database')

  args = parser.parse_args()

  header = 'expid','detid','pa_lyr','se_lyr','pa_aqr','se_aqr','ra','dec'
  db = pd.read_csv(args.database, sep=' ', names=header, skiprows=11)

  print('## Separation Angle from the shower radiants')
  print('#')
  print('# 1st column: exposure ID')
  print('# 2nd column: detector ID')
  print('# 3rd column: separation from Apr Lyrids radiant')
  print('# 4th column: separation from Eta Aquariids radiant')
  print('')
  with open(args.meteors) as meteors:
    fmt = int, int, float, float, float, float
    for line in meteors:
      if line[0] == '#': continue
      if line.strip() == '': continue

      vals = [f(v) for f,v in zip(fmt, line.split())]
      expid, detid, pa = vals[0], vals[1]+1, vals[5]
      sel = (db.expid == expid) & (db.detid == detid)
      dpa_lyr = pa - db[sel].iloc[0].pa_lyr
      dpa_aqr = pa - db[sel].iloc[0].pa_aqr
      se_lyr = db[sel].iloc[0].se_lyr
      se_aqr = db[sel].iloc[0].se_aqr
      arc_lyr = minarc(se_lyr,dpa_lyr)
      arc_aqr = minarc(se_aqr,dpa_aqr)
      print('{:5d} {:1d} {:7.3f} {:7.3f}'.format(expid,detid,arc_lyr,arc_aqr))
