#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division
from argparse import ArgumentParser as ap
import astropy.time as time
import astropy.units as u
import astropy.coordinates as coo
import numpy as np
import warnings


apr_lyrids = \
  coo.SkyCoord(ra=271.4*u.degree, dec=33.6*u.degree, frame='icrs')
eta_aquariids = \
  coo.SkyCoord(ra=338.0*u.degree, dec=-1.0*u.degree, frame='icrs')


if __name__ == '__main__':
  parser = ap('calculate position angle of meteor showars')
  parser.add_argument('coo', help='coordinate file')

  args = parser.parse_args()

  sep = 64.700
  offset = [226.45-sep*n for n in range(4)]
  offset.extend(map(lambda x: -x, offset[::-1]))
  offset = map(lambda x: x*u.arcmin, offset)

  print('## position angles to Apr Lyrids and Eta Aquariids')
  print('#')
  print('# 1st column: exposure ID')
  print('# 2nd column: detector ID')
  print('# 3rd column: position angle to Apr Lyrids')
  print('# 4th column: separation angle to Apr Lyrids')
  print('# 5th column: position angle to Eta Aquariids')
  print('# 6th column: separation angle to Eta Aquariids')
  print('# 7th column: RA position of the detector')
  print('# 8th column: Dec position of the detector')
  print('#')
  with open(args.coo) as input_file:
    for line in input_file:
      elem = line.split()
      expid = elem[0]
      for n,off in enumerate(offset):
        ra  = coo.Longitude(coo.Angle(elem[1], unit=u.hourangle)+off)
        dec = coo.Latitude(elem[2], unit=u.degree)
        chip = coo.SkyCoord(ra=ra, dec=dec, frame='icrs')
        pa_lyrids = chip.position_angle(apr_lyrids).to(u.degree)
        se_lyrids = chip.separation(apr_lyrids).to(u.degree)
        pa_aquariids = chip.position_angle(eta_aquariids).to(u.degree)
        se_aquariids = chip.separation(eta_aquariids).to(u.degree)
        print(expid, n+1,
              pa_lyrids.to_string(decimal=True, precision=4),
              se_lyrids.to_string(decimal=True, precision=4),
              pa_aquariids.to_string(decimal=True, precision=4),
              se_aquariids.to_string(decimal=True, precision=4),
              ra.to_string(pad=True, precision=1, sep=':'),
              dec.to_string(pad=True, precision=1, sep=':'))
